<?php

//Defaults
define('SCC_REFRESH_ON_CONSENT',1);
define('SCC_STYLE','dark');
define('SCC_BANNER_POSITION','top');
define('SCC_TAG_POSITION','bottom-right');
define('SCC_USE_SSL',false);
define('SCC_CONSENT_TYPE','explicit');
define('SCC_ONLY_SHOW_BANNER_ONCE',false);
define('SCC_HIDE_ALL_SITES_BUTTON',false);
define('SCC_DISABLE_ALL_SITES',false);
define('SCC_HIDE_PRIVACY_SETTINGS_TAB',false);
define('SCC_TESTMODE',false);
define('SCC_OVERRIDE_WARNINGS',false);
define('SCC_GROUPS','');

function silktidecc_settings_form($form, &$form_state)
{
  $form['scc_style'] = array(
    '#title' => 'Style',
    '#type' => 'select',
    '#options' => array('light' => 'Light', 'dark' => 'Dark'),
    '#default_value' => variable_get('scc_style',SCC_STYLE)
  );

  $form['scc_banner_position'] = array(
    '#title' => 'Banner position',
    '#description' => 'A string denoting which style to use.',
    '#type' => 'select',
    '#options' => array('top' => 'Top', 'bottom' => 'Bottom', 'push' => 'Push (expirimental)'),
    '#default_value' => variable_get('scc_banner_position',SCC_BANNER_POSITION)
  );

  $form['scc_tag_position'] = array(
    '#title' => 'Banner position',
    '#description' => 'The position of the \'privacy settings\' tab.',
    '#type' => 'select',
    '#options' => array(
      'bottom-right' => 'Bottom right',
      'bottom-left' => 'Bottom left',
      'vertical-left' => 'Vertical left',
      'vertical-right' => 'Vertical right'
    ),
    '#default_value' => variable_get('scc_tag_position',SCC_TAG_POSITION)
  );

  $form['scc_consent_type'] = array(
    '#title' => 'Consent type',
    '#description' => 'Should the plugin use explicit or implied consent.',
    '#type' => 'select',
    '#options' => array(
      'explicit' => 'Explicit',
      'Implicit' => 'Implicit'
    ),
    '#default_value' => variable_get('scc_consent_type',SCC_CONSENT_TYPE),
    '#ajax' => array(
      'callback' => 'silktidecc_settings_ajax',
      'wrapper' => 'showbanneronlyonce_wrapper',
    ),
  );

  $form['scc_refresh_on_consent'] = array(
    '#title' => 'Refresh on consent',
    '#description' => 'When set to true, the plugin will refresh the page after a change in consent.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_refresh_on_consent',SCC_REFRESH_ON_CONSENT)
  );

  $form['scc_use_ssl'] = array(
    '#title' => 'Use ssl',
    '#description' => 'Whether or not the plugin should use SSL',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_use_ssl',SCC_USE_SSL)
  );

  $form['scc_only_show_banner_once'] = array(
    '#prefix' => '<div id="showbanneronlyonce_wrapper">',
    '#suffix' => '</div>',
    '#title' => 'Only show banner once',
    '#description' => 'This option can only be used when consenttype is set to "implicit". The consent slide-down notification will only be shown once if set to true even if the visitor does not respond to the banner (on subsequent pages, just the privacy settings tab will be shown).',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_only_show_banner_once',SCC_ONLY_SHOW_BANNER_ONCE)
  );

  $form['scc_hide_all_sites_button'] = array(
    '#title' => 'Hide all sites button',
    '#description' => 'The "Save for all sites"/"Accept for all sites" button can be hidden.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_hide_all_sites_button',SCC_HIDE_ALL_SITES_BUTTON)
  );

  $form['scc_disable_all_sites'] = array(
    '#title' => 'Disable all sites',
    '#description' => 'This disables all of the "all sites" features within the plugin. Visitors will have to explicitly give consent on your site.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_disable_all_sites',SCC_DISABLE_ALL_SITES)
  );

  $form['scc_hide_privacy_settings_tab'] = array(
    '#title' => 'Hide privacy settings tab',
    '#description' => 'The privacy tab can be hidden.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_hide_privacy_settings_tab',SCC_HIDE_PRIVACY_SETTINGS_TAB)
  );

  $form['scc_testmode'] = array(
    '#title' => 'Testmode',
    '#description' => 'The plugin can be forced into test mode, accepting all cookies or declining all cookies (not for production use).',
    '#type' => 'select',
    '#options' => array(
      'false' => 'Off',
      'accept' => 'Accept',
      'decline' => 'Decline'
    ),
    '#default_value' => variable_get('scc_testmode',SCC_TESTMODE)
  );

  $form['scc_override_warnings'] = array(
    '#title' => 'Override warnings',
    '#description' => 'By default, the plugin will warn the developer if no code blocks are found. This is to aid developers when setting up the plugin, but it can also be normal (e.g. the plugin is added to all pages, but one page of the website doesn\'t have any cookie setting scripts)',
    '#type' => 'checkbox',
    '#default_value' => variable_get('scc_override_warnings',SCC_OVERRIDE_WARNINGS)
  );

  if(isset($form_state['values']['scc_consent_type']) && $form_state['values']['scc_consent_type'] == 'implicit')
  {
    $form['scc_onlyshowbanneronce']['#disabled'] = true;
  }

  return system_settings_form($form);
}

function silktidecc_settings_ajax($form, $form_state)
{
  return $form['scc_onlyshowbanneronce'];
}

function silktidecc_settings()
{
  $settings = array(
    'style' => variable_get('scc_style',SCC_STYLE),
    'refreshOnConsent' => variable_get('scc_refresh_on_consent',SCC_REFRESH_ON_CONSENT),
    'bannerPosition' => variable_get('scc_banner_position',SCC_BANNER_POSITION),
    'tagPosition' => variable_get('scc_tag_position',SCC_TAG_POSITION),
    'useSSL' => variable_get('scc_use_ssl',SCC_USE_SSL),
    'consenttype' => variable_get('scc_consent_type',SCC_CONSENT_TYPE),
    'onlyshowbanneronce' => variable_get('scc_only_show_banner_once',SCC_ONLY_SHOW_BANNER_ONCE),
    'hideallsitesbutton' => variable_get('scc_hide_all_sites_button',SCC_HIDE_ALL_SITES_BUTTON),
    'disableallsites' => variable_get('scc_disable_all_sites',SCC_DISABLE_ALL_SITES),
    'hideprivacysettingstab' => variable_get('scc_hide_privacy_settings_tab',SCC_HIDE_PRIVACY_SETTINGS_TAB),
    'testmode' => variable_get('scc_testmode',SCC_TESTMODE),
    'overridewarnings' => variable_get('scc_override_warnings',SCC_OVERRIDE_WARNINGS)
  );

  return $settings;
}